DROP TABLE IF EXISTS genes;

CREATE TABLE `genes` (
  `id`           INT(11) NOT NULL AUTO_INCREMENT,
  `genbank_id`   VARCHAR(10)      DEFAULT NULL,
  `abbreviation` VARCHAR(100)     DEFAULT NULL,
  `name`         VARCHAR(100)     DEFAULT NULL,
  `process`      VARCHAR(100)     DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `genbank_id` (`genbank_id`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

