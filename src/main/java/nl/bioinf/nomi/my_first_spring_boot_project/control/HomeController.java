package nl.bioinf.nomi.my_first_spring_boot_project.control;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.Locale;

/**
 * Created by michiel on 15/02/2017.
 */
@Controller
public class HomeController {
    @RequestMapping(
            value = {"", "/", "/index", "/home.html", "/home"}, method = RequestMethod.GET)
    public String home(Model model, Locale locale) {
        return "redirect:" + locale.getLanguage() + "/home";
    }

    @RequestMapping(value = "/{locale}/home")
    public String homeWithLocale() {
        return "index";
    }
}
